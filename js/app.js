
document.addEventListener('DOMContentLoaded', function(event) {


    $('[data-toggle="tooltip"]').tooltip()

    $('[data-toggle="popover"]').popover({
        trigger: 'focus'
    })

    $('.carousel').carousel({
        interval: 1800
    })

    $('#contacto').on('show.bs.modal', function(e) {
        $('#btn-contacto').removeClass('badge-secondary').addClass('btn-dark').prop('disable', true);
    })

    $('#contacto').on('shown.bs.modal', function(e) {
        console.log("Probando la terminación del despliegue");
        
    })

    $('#contacto').on('hide.bs.modal', function(e) {
        console.log("Probando el inicio del cierre del modal");
    })

    $('#contacto').on('hidden.bs.modal', function(e) {
        $('#btn-contacto').removeClass('btn-dark').addClass('badge-secondary').prop('disable', false);
    })

})
